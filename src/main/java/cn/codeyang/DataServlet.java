package cn.codeyang;

import cn.codeyang.domain.Department;
import net.sf.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.RowSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by yangzhongyang on 16/8/11
 */
public class DataServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int limit = Integer.valueOf(req.getParameter("limit"));
        int offset = Integer.valueOf(req.getParameter("offset"));
        String departmentname = req.getParameter("departmentname");
        String status = req.getParameter("statu");

        Department department1 = new Department(1, "销售部", "1", "一级部门, 非常重要", "没有上级部门");
        Department department2 = new Department(2, "技术部", "0", "超级部门, 更加重要", "没有上级部门");
        Department department3 = new Department(3, "垃圾部", "-1", "垃圾部门,,,", "没有上级部门");
        List<Department> departmentList = Arrays.asList(department1, department2, department3);

        int total = departmentList.size();

        List<Department> rowList = new ArrayList<Department>();
        for (int i = 0; i < limit; i++){
            try {
                rowList.add(departmentList.get(offset + i));
            }catch (Exception e){}
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.accumulate("total", total);
        jsonObject.accumulate("rows", rowList);
        jsonObject.accumulate("department", departmentList);
        System.out.println(jsonObject.toString());

        resp.setContentType("text/html;charset=utf-8");
        resp.getWriter().write(jsonObject.toString());

    }
}
