package cn.codeyang.domain;

/**
 * Created by yangzhongyang on 16/8/11
 */
public class Department {
    private Integer id;
    private String name;
    private String level;
    private String desc;
    private String parentName;

    public Department() {
    }

    public Department(Integer id, String name, String level, String desc, String parentName) {
        this.id = id;
        this.name = name;
        this.level = level;
        this.desc = desc;
        this.parentName = parentName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", level='" + level + '\'' +
                ", desc='" + desc + '\'' +
                '}';
    }
}
